/* http://www.yolinux.com/TUTORIALS/ForkExecProcesses.html */

#include <stdio.h>
#include <unistd.h>

int main(void)
{
  int salida;

  /*    ruta               $0                $1    $2    $3   \0         */
  salida = execl("/bin/no-existe", "/bin/no-existe", "-r", "-t", "-l", (char *) 0);

  /* Se llega a estas lineas en caso de que exec(3) tenga errores */
  printf("La salida de execl fue: %d\n", salida);
  return salida;
}

