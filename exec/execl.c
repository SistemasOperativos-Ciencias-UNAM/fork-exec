/* http://www.yolinux.com/TUTORIALS/ForkExecProcesses.html */

#include <stdio.h>
#include <unistd.h>

int main(void)
{
  printf("Empezando el programa\n");

  /*    ruta        $0         $1    $2    $3   \0         */
  execl("/bin/ls", "/bin/ls", "-r", "-t", "-l", (char *) 0);

  /* Estas dos lineas no deberian de ejecutarse */
  /* Solo puedo llegar a ellas si exec(3) tuvo algun error */
  printf("Terminando el programa\n");
  return 0;
}
