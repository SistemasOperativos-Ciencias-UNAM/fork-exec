/* man 7 credentials */

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>

int main(void)
{
  pid_t pid = getpid();

  printf("Identificador de proceso\n");
  printf("getpid(2)	PID:	%d\n\n", getpid());

  printf("Identificador de proceso padre\n");
  printf("getppid(2)	PPID:	%d\n\n", getppid());

  printf("Identificador de sesión\n");
  printf("getsid(2)	SID:	%d\n\n", getsid(pid));

  printf("Identificador de grupo de procesos\n");
  printf("getpgrp(2)	PGRP:	%d\n\n", getpgrp());

  printf("Identificador de usuario\n");
  printf("getuid(2)	UID:	%d\n\n", getuid());

  printf("Identificador de grupo\n");
  printf("getgid(2)	GID:	%d\n\n", getgid());

  sleep(UINT_MAX);
  return 0;
}
